package br.gov.sp.fatec.vanborabackend.model.route;

import java.io.Serializable;

public class Point implements Serializable {

    private static final long serialVersionUID = 9203189789991814360L;

    public Point() {
    }

    private float latitude;
    private float longitude;

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

}
