package br.gov.sp.fatec.vanborabackend.model.route;

import java.io.Serializable;
import java.util.List;

public class Route implements Serializable {

    private static final long serialVersionUID = 1511194132179698803L;

    public Route() {
    }

    private Summary summary;
    private List<Leg> legs;

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

}
