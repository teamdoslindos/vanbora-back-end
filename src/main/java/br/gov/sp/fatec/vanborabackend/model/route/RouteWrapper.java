package br.gov.sp.fatec.vanborabackend.model.route;

import java.util.List;

public class RouteWrapper {

    public RouteWrapper() {
    }

    private List<Route> routes;

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

}
