package br.gov.sp.fatec.vanborabackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import br.gov.sp.fatec.vanborabackend.model.Van;
import br.gov.sp.fatec.vanborabackend.service.VanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@RestController
@RequestMapping(path = "van")
public class VanController {

    @Autowired
    private VanService vanService;

    @ApiOperation(
        value = "Finds a van",
        notes = "Searches by Plate"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Van found"),
        @ApiResponse(code = 404, message = "Van not found")
    })
    @GetMapping("{van_id}")
    public Van find(@PathVariable("van_id") final Integer vanId) {
        return vanService.findById(vanId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @ApiOperation(
            value = "Finds a vans list",
            notes = "Searches by user of type 'driver'"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Vans found"),
            @ApiResponse(code = 404, message = "Vans or driver not found")
    })
    @GetMapping("/byDriver/{driver_id}")
    public List<Van> findAllByDriver(@PathVariable("driver_id") final Integer driverId) {
        return  vanService.findAllByDriver(driverId);
    }

    @ApiOperation(
        value = "Saves a new van",
        notes = "Fails if van ID or plate already taken, or if driver is of incorrect user type"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Van saved"),
        @ApiResponse(code = 404, message = "Driver not found"),
        @ApiResponse(code = 422, message = "Van ID or plate already taken, or driver is of incorrect user type")
    })
    @PostMapping
    public Van save(@RequestBody final Van van)
    {
        return vanService.save(van);
    }
    
    @ApiOperation(
        value = "Updates a van",
        notes = "Searches by ID or plate"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Van updated"),
        @ApiResponse(code = 404, message = "Van or driver not found"),
        @ApiResponse(code = 422, message = "Van plate already taken, or driver is of incorrect user type")
    })
    @PatchMapping
    public Van update(@RequestBody final Van van) {
        return vanService.update(van);
    }

    @ApiOperation(
        value = "Deletes a van",
        notes = "Searches by plate"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Van deleted"),
        @ApiResponse(code = 404, message = "Van not found")
    })
    @DeleteMapping
    public Van delete(@RequestBody Van van) {
        return vanService.delete(van);
    }

}
