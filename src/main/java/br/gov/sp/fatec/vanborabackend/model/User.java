package br.gov.sp.fatec.vanborabackend.model;

import java.util.Date;

import javax.persistence.*;

import br.gov.sp.fatec.vanborabackend.serializer.AddressDeserializer;
import br.gov.sp.fatec.vanborabackend.serializer.AddressSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "app_user")
public class User {

    @Id
    // Generated value strategy required to make Hibernate let PostgreSQL
    // generate primary key.
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String email;

    private String password;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDate = new Date();

    private String phone;

    private String cpf;

    private String cnh;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "address_id")
    @JsonDeserialize(using = AddressDeserializer.class)
    @JsonSerialize(using = AddressSerializer.class)
    private Address address;

    // Default contructor required by Servlet.
    public User() {
        // Empty.
    }

    @JsonIgnore
    public boolean isDriver() {
        return getCnh() != null;
    }

    // Returns `false` if an invalid set is made.
    @JsonIgnore
    public boolean update(final User user) {
        if (user.getName() != null) {
            setName(user.getName());
        }

        if (user.getEmail() != null) {
            setEmail(user.getEmail());
        }

        if (user.getPassword() != null) {
            setPassword(user.getPassword());
        }

        if (user.getBirthDate() != null) {
            setBirthDate(user.getBirthDate());
        }

        if (user.getPhone() != null) {
            setPhone(user.getPhone());
        }

        if (getCnh() != null) {
            if (user.getCpf() != null) {
                setCpf(user.getCpf());
            }

            if (user.getCnh() != null) {
                setCnh(user.getCnh());
            }
        } else {
            if (user.getCpf() != null || user.getCnh() != null) {
                return false;
            }
        }

        if (user.getAddress() != null) {
            setAddress(user.getAddress());
        }

        return true;
    }

    @JsonView(Group.View.class)
    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    @JsonView(Group.View.class)
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(final Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(final String cpf) {
        this.cpf = cpf;
    }

    public String getCnh() {
        return cnh;
    }

    public void setCnh(final String cnh) {
        this.cnh = cnh;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
