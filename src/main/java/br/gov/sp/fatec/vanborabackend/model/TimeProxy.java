package br.gov.sp.fatec.vanborabackend.model;

import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TimeProxy {

    @JsonFormat(pattern = "HH:mm", locale = "pt-BR", timezone = "Brazil/East")
    private Calendar time;

    public TimeProxy() {
        // Empty.
    }

    public TimeProxy(final Calendar time) {
        setTime(time);
    }

    public Calendar getTime() {
        return time;
    }

    public void setTime(final Calendar time) {
        this.time = time;
    }

}
