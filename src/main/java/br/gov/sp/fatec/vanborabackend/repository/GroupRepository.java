package br.gov.sp.fatec.vanborabackend.repository;

import br.gov.sp.fatec.vanborabackend.model.Group;
import br.gov.sp.fatec.vanborabackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<Group, Integer> {

    Optional<Group> findById(Integer id);

    Optional<Group> findFirstByDriverAndName(User driver, String name);

    Optional<List<Group>> findAllByDriver(User driver);

    Optional<List<Group>> findAllByPassengersUser(User passengerUser);

}
