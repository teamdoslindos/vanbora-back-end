package br.gov.sp.fatec.vanborabackend.controller;
import br.gov.sp.fatec.vanborabackend.model.Group;
import br.gov.sp.fatec.vanborabackend.model.TimeProxy;
import br.gov.sp.fatec.vanborabackend.service.GroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Api
@RestController
@RequestMapping(path = "group")
public class GroupController {

    @Autowired
    private GroupService groupService;

    @ApiOperation(
            value = "Finds a group",
            notes = "Searches by ID or by driver and name"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Group found"),
            @ApiResponse(code = 404, message = "Group not found")
    })
    @GetMapping("{group_id}")
    public Group find(@PathVariable("group_id") final Integer groupId) {
        return groupService.findById(groupId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @ApiOperation(
            value = "Finds all groups saved with the requested driver",
            notes = "Searches by ID of driver"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Groups found"),
            @ApiResponse(code = 404, message = "No Group found")
    })
    @GetMapping("byDriver/{driver_id}")
    public List<Group> findAllByDriver(@PathVariable("driver_id") final Integer driverId) {
        return groupService.findAllByDriver(driverId);
    }

    @ApiOperation(
            value = "Finds all groups saved which the requested passenger is part of",
            notes = "Searches by ID of passenger"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Groups found"),
            @ApiResponse(code = 404, message = "No Group found")
    })
    @GetMapping("byPassenger/{passenger_id}")
    public List<Group> findAllByPassenger(@PathVariable("passenger_id") final Integer passengerId) {
        return groupService.findAllByPassenger(passengerId);
    }

    @ApiOperation(
            value = "Saves a new group",
            notes = "Fails if the ID or driver and name attributes are already taken, or if any of the listed " +
                    "passengers and driver cannot be found or are of incorrect user types"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Group found"),
            @ApiResponse(code = 422, message = "Group ID or driver and name already taken, or one of the passengers" +
                    " or driver cannot be found, or are of incorrect user types")
    })
    @PostMapping
    public Group save(@RequestBody final Group group) {
        return groupService.save(group);
    }

    @ApiOperation(
            value = "Updates a group",
            notes = "Searches by ID or by driver and name, and verifies if all listed passengers and driver are saved" +
                    " and have the correct user types"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Group updated"),
            @ApiResponse(code = 404, message = "Group not found"),
            @ApiResponse(code = 422, message = "Driver or one of the passengers cannot be found, or are of " +
                    "incorrect user types")
    })
    @PatchMapping
    public Group update(@RequestBody final Group group) {
        return groupService.update(group);
    }

    @PostMapping("{group_id}/addPassenger/{user_id}")
    public Group savePassenger(@PathVariable("group_id") final Integer groupId,
                               @PathVariable("user_id") final Integer userId) {
        return groupService.savePassenger(groupId, userId);
    }

    @ApiOperation(
        value = "Deletes a group",
        notes = "Searches by ID"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Group deleted"),
        @ApiResponse(code = 404, message = "Group not found")
    })
    @DeleteMapping
    public Group delete(@RequestBody final Group group) {
        return groupService.delete(group);
    }


    @PostMapping("{group_id}/changeDepartureTime/{user_id}")
    public Group changeDepartureTime(@PathVariable("group_id") final Integer groupId,
                                     @PathVariable("user_id") final Integer userId,
                                     @RequestBody TimeProxy time) {
        return groupService.changeDepartureTime(groupId, userId, time);
    }

    @PostMapping("{group_id}/changeReturnTime/{user_id}")
    public Group changeReturnTime(@PathVariable("group_id") final Integer groupId,
                                  @PathVariable("user_id") final Integer userId,
                                  @RequestBody TimeProxy time) {
        return groupService.changeReturnTime(groupId, userId, time);
    }


    @PostMapping("{group_id}/confirmReturn/{user_id}")
    public Group confirmReturn(@PathVariable("group_id") final Integer groupId,
                               @PathVariable("user_id") final Integer userId) {
        return groupService.confirmReturn(groupId, userId);
    }

    @PostMapping("{group_id}/confirmDeparture/{user_id}")
    public Group confirmDeparture(@PathVariable("group_id") final Integer groupId,
                                  @PathVariable("user_id") final Integer userId) {
        return groupService.confirmDeparture(groupId, userId);
    }
}
