package br.gov.sp.fatec.vanborabackend.service;

import java.util.Optional;

import br.gov.sp.fatec.vanborabackend.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import br.gov.sp.fatec.vanborabackend.model.User;
import br.gov.sp.fatec.vanborabackend.repository.UserRepository;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressService addressService;

    private Optional<User> findOptional(final User user) {
        if (user == null) {
            return Optional.empty();
        }

        if (user.getId() != null) {
            return userRepository.findById(user.getId());
        } else {
            return userRepository.findByEmail(user.getEmail());
        }
    }

    public User find(final User user) {
        return findOptional(user).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Optional<User> findById(final Integer id) {
        return userRepository.findById(id);
    }

    public User save(final User user) {
        // XXX: The address MUST be sent. There should be a better way to do this.
        if (findOptional(user).isPresent() || user.getAddress() == null) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        final Address foundAddress = addressService.findOptional(user.getAddress())
                .orElseGet(() -> addressService.save(user.getAddress()));

        user.setAddress(foundAddress);

        return userRepository.save(user);
    }

    public User update(final User user) {
        final User oldUser = find(user);

        // `update` returns `false` if an invalid set was made.
        if (!oldUser.update(user)) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return userRepository.save(oldUser);
    }

    public User delete(final User user) {
        final User olduser = find(user);

        userRepository.deleteById(olduser.getId());

        return olduser;
    }

    public User login(final User user) {
        user.setId(null);

        if (user.getEmail() == null || user.getPassword() == null) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        final User foundUser = find(user);

        if (!foundUser.getPassword().equals(user.getPassword())) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return foundUser;
    }

}
