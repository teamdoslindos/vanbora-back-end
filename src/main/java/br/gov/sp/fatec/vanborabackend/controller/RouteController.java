package br.gov.sp.fatec.vanborabackend.controller;

import br.gov.sp.fatec.vanborabackend.model.route.RouteWrapper;
import org.springframework.web.client.RestTemplate;

public class RouteController {

    public static RouteWrapper makeOrderedRouteRequest (String uri) {
        return new RestTemplate().getForObject(uri, RouteWrapper.class);
    }
}
