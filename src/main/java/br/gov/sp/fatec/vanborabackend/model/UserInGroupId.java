package br.gov.sp.fatec.vanborabackend.model;

import java.io.Serializable;

public class UserInGroupId implements Serializable {

    private static final long serialVersionUID = 6556613076284658413L;

    private Integer user;

    private Integer group;

    public UserInGroupId(Integer user, Integer group) {
        this.setUser(user);
        this.setGroup(group);
    }

    public UserInGroupId() {
        // Empty.
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(final Integer user) {
        this.user = user;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(final Integer group) {
        this.group = group;
    }

}
