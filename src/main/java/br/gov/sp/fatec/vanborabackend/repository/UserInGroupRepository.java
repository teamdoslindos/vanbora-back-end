package br.gov.sp.fatec.vanborabackend.repository;

import br.gov.sp.fatec.vanborabackend.model.UserInGroup;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInGroupRepository extends JpaRepository<UserInGroup, Integer> {
    
    // Empty.

}
