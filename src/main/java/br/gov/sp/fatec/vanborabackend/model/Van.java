package br.gov.sp.fatec.vanborabackend.model;

import javax.persistence.*;

@Entity
@Table(name = "van")
public class Van {

    @Id
    // Generated value strategy required to make Hibernate let PostgreSQL
    // generate primary key.
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "driver_id")
    private User driver;

    private String plate;

    private String color;

    @Column(name = "qty_seats")
    private Integer quantityOfSeats;

    private String model;

    @Lob
    @Column(name = "photo", columnDefinition="BLOB")
    private byte[] photo;

    // Default contructor required by Servlet.
    public Van() {
        // Empty.
    }

    public Integer getId() {
        return id;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        // Setter protected against null values to simplify business logic.
        if (driver != null) {
            this.driver = driver;
        }
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        // Setter protected against null values to simplify business logic.
        if (plate != null) {
            this.plate = plate;
        }
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        // Setter protected against null values to simplify business logic.
        if (color != null) {
            this.color = color;
        }
    }

    public Integer getQuantityOfSeats() {
        return quantityOfSeats;
    }

    public void setQuantityOfSeats(Integer quantityOfSeats) {
        // Setter protected against null values to simplify business logic.
        if (quantityOfSeats != null) {
            this.quantityOfSeats = quantityOfSeats;
        }
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        // Setter protected against null values to simplify business logic.
        if (model != null) {
            this.model = model;
        }
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        // Setter protected against null values to simplify business logic.
        if (photo != null) {
            this.photo = photo;
        }
    }

}
