package br.gov.sp.fatec.vanborabackend.serializer;

import br.gov.sp.fatec.vanborabackend.model.Address;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class AddressDeserializer extends StdDeserializer<Address> {

    private static final long serialVersionUID = -1773908089137510510L;

    public AddressDeserializer() {
        this(null);
    }

    public AddressDeserializer(Class<?> address) {
        super(address);
    }

    @Override
    public Address deserialize(JsonParser parser, DeserializationContext context)
            throws IOException, JsonProcessingException {

        Address deserializedAdress = new Address();

        JsonNode mainNode = parser.getCodec().readTree(parser);
        JsonNode addressNode = mainNode.get("address");
        JsonNode positionNode = mainNode.get("position");

        deserializedAdress.setStreet(addressNode.get("streetName").asText());
        deserializedAdress.setNeighborhood(addressNode.get("municipalitySubdivision").asText());
        deserializedAdress.setCity(addressNode.get("municipality").asText());
        deserializedAdress.setNumber(addressNode.get("streetNumber").asInt());

        deserializedAdress.setLatitude((float) positionNode.get("lat").asDouble());
        deserializedAdress.setLongitude((float) positionNode.get("lon").asDouble());

        return deserializedAdress;
    }
}

