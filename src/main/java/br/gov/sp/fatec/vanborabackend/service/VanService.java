package br.gov.sp.fatec.vanborabackend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import br.gov.sp.fatec.vanborabackend.model.User;
import br.gov.sp.fatec.vanborabackend.model.Van;
import br.gov.sp.fatec.vanborabackend.repository.VanRepository;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class VanService {

    @Autowired
    private VanRepository vanRepository;

    @Autowired
    private UserService userService;

    private Optional<Van> findOptional(final Van van) {
        if (van == null) {
            return Optional.empty();
        }

        if (van.getId() != null) {
            return vanRepository.findById(van.getId());
        } else {
            return vanRepository.findByPlate(van.getPlate());
        }
    }

    public Van find(final Van van) {
        return findOptional(van).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Optional<Van> findById(final Integer vanId) {
        return vanRepository.findById(vanId);
    }

    public List<Van> findAllByDriver(final Integer driverId) {
        final User driver = userService.findById(driverId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        return vanRepository.findAllByDriver(driver)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Van save(final Van van) {

        User foundDriver = userService.find(van.getDriver());
        if (foundDriver.isDriver()) {
            van.setDriver(foundDriver);
        } else {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        if (findOptional(van).isPresent()) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return this.vanRepository.save(van);
    }

    public Van update(final Van van) {

        if (van.getDriver() != null) {
            User foundDriver = userService.find(van.getDriver());
            if (foundDriver.isDriver()) {
                van.setDriver(foundDriver);
            } else {
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }

        Optional<Van> existingVanWithNewPlate = null;

        if (van.getPlate() != null) {
            existingVanWithNewPlate = vanRepository.findByPlate(van.getPlate());
        }

        if (existingVanWithNewPlate != null && existingVanWithNewPlate.isPresent()) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
            final Van oldVan = find(van);
            oldVan.setColor(van.getColor());
            oldVan.setDriver(van.getDriver());
            oldVan.setModel(van.getModel());
            oldVan.setPhoto(van.getPhoto());
            oldVan.setPlate(van.getPlate());
            oldVan.setQuantityOfSeats(van.getQuantityOfSeats());
            return vanRepository.save(oldVan);
        }

    }

    public Van delete(Van van) {
        final Van foundVan = findOptional(van).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        vanRepository.delete(foundVan);

        return foundVan;
    }

}
