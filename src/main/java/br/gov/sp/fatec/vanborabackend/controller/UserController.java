package br.gov.sp.fatec.vanborabackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import br.gov.sp.fatec.vanborabackend.model.User;
import br.gov.sp.fatec.vanborabackend.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@RestController
@RequestMapping(path = "user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(
        value = "Finds a user",
        notes = "Searches by ID or email"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User found"),
        @ApiResponse(code = 404, message = "User not found")
    })
    @GetMapping("{user_id}")
    public User find(@PathVariable("user_id") final Integer userId) {
        return userService.findById(userId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @ApiOperation(
        value = "Saves a new user",
        notes = "Fails if the ID or email provided is already taken"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User found"),
        @ApiResponse(code = 422, message = "User ID or email already taken")
    })
    @PostMapping
    public User save(@RequestBody final User user) {
        return userService.save(user);
    }

    @ApiOperation(
        value = "Updates a user",
        notes = "Searches by ID or email"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User updated"),
        @ApiResponse(code = 404, message = "User not found")
    })
    @PatchMapping
    public User update(@RequestBody final User user) {
        return userService.update(user);
    }

    @ApiOperation(
        value = "Deletes a user",
        notes = "Searches by ID or email"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User deleted"),
        @ApiResponse(code = 404, message = "User not found")
    })
    @DeleteMapping
    public User delete(@RequestBody final User user) {
        return userService.delete(user);
    }

    @PostMapping("login")
    public User login(@RequestBody final User user) {
        return userService.login(user);
    }

}
