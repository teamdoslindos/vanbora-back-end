package br.gov.sp.fatec.vanborabackend.model;

import java.util.Calendar;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import java.util.Objects;

@Entity
@Table(name = "user_group")
@IdClass(UserInGroupId.class)
public class UserInGroup implements Comparable {

    @Id
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "user_id")
    private User user;

    @Id
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "group_id")
    @JsonIgnore
    private Group group;

    private boolean departureConfirmed;

    private boolean returnConfirmed;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", locale = "pt-BR", timezone = "Brazil/East")
    private Calendar departureTime;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", locale = "pt-BR", timezone = "Brazil/East")
    private Calendar returnTime;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", locale = "pt-BR", timezone = "Brazil/East")
    private Calendar optimalDeparture;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", locale = "pt-BR", timezone = "Brazil/East")
    private Calendar optimalReturn;

    @JsonIgnore
    @Column(name = "passengers_order")
    private Integer orderPosition;


    // Default contructor required by Servlet.
    public UserInGroup() {
        // Empty.
    }

    public UserInGroup(final User user, final Group group) {
        this.setUser(user);
        this.setGroup(group);
        this.setDepartureConfirmed(true);
        this.setReturnConfirmed(true);
    }

    public boolean getDepartureConfirmed() {
        return departureConfirmed;
    }

    public void setDepartureConfirmed(boolean departureConfirmed) {
        this.departureConfirmed = departureConfirmed;
    }

    public boolean getReturnConfirmed() {
        return returnConfirmed;
    }

    public void setReturnConfirmed(boolean returnConfirmed) {
        this.returnConfirmed = returnConfirmed;
    }

    public Calendar getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Calendar departureTime) {
        this.departureTime = departureTime;
    }

    public Calendar getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Calendar returnTime) {
        this.returnTime = returnTime;
    }

    public Calendar getOptimalDeparture() {
        return optimalDeparture;
    }

    public void setOptimalDeparture(Calendar optimalDeparture) {
        this.optimalDeparture = optimalDeparture;
    }

    public Calendar getOptimalReturn() {
        return optimalReturn;
    }

    public void setOptimalReturn(Calendar optimalReturn) {
        this.optimalReturn = optimalReturn;
    }

    @JsonView(Group.View.class)
    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(final Group group) {
        this.group = group;
    }

    public Integer getOrderPosition() {
        return orderPosition;
    }

    public void setOrderPosition(Integer orderPosition) {
        this.orderPosition = orderPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInGroup userGroup = (UserInGroup) o;
        return user.getId().equals(userGroup.user.getId()) &&
                group.getId().equals(userGroup.group.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(user.getId(), group.getId());
    }

    public int compareTo(Object o) {
        if (!(o instanceof UserInGroup)) {
            return 0;
        }
        UserInGroup comparedUserInGroup = (UserInGroup) o;
        if (this.getOrderPosition() == null || comparedUserInGroup.getOrderPosition() == null){
            return 0;
        }
        return this.getOrderPosition().compareTo(comparedUserInGroup.getOrderPosition());
    }

}
