package br.gov.sp.fatec.vanborabackend.service;

import br.gov.sp.fatec.vanborabackend.model.Address;
import br.gov.sp.fatec.vanborabackend.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    public Optional<Address> findOptional(final Address address) {
        if (address == null) {
            return Optional.empty();
        }

        if (address.getId() != null) {
            return addressRepository.findById(address.getId());
        } else {
            return addressRepository.findFirstByStreetAndNumber(address.getStreet(), address.getNumber());
        }
    }

    public Address find(Address address){
        return findOptional(address).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Address findById(Integer addressId){
        return addressRepository.findById(addressId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Address save(final Address address) {
        if (findOptional(address).isPresent()) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return addressRepository.save(address);
    }


}
