package br.gov.sp.fatec.vanborabackend.service;

import br.gov.sp.fatec.vanborabackend.model.Address;
import br.gov.sp.fatec.vanborabackend.model.Group;
import br.gov.sp.fatec.vanborabackend.model.TimeProxy;
import br.gov.sp.fatec.vanborabackend.model.User;
import br.gov.sp.fatec.vanborabackend.model.UserInGroup;
import br.gov.sp.fatec.vanborabackend.repository.GroupRepository;
import br.gov.sp.fatec.vanborabackend.repository.UserInGroupRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserInGroupRepository userGroupRepository;

    @Autowired
    private AddressService addressService;

    @Autowired
    private UserService userService;

    @Autowired
    private RouteProvider routeProvider;

    private Optional<Group> findOptional(final Group group) {
        if (group == null) {
            return Optional.empty();
        }

        if (group.getId() != null) {
            return groupRepository.findById(group.getId());
        } else {
            return groupRepository.findFirstByDriverAndName(group.getDriver(), group.getName());
        }
    }

    public Group find(final Group group) {
        return findOptional(group).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Optional<Group> findById(final Integer groupId) {
        return groupRepository.findById(groupId);
    }

    public List<Group> findAllByDriver(final Integer driverId){
        final User driver = userService.findById(driverId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        return groupRepository.findAllByDriver(driver)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public List<Group> findAllByPassenger(final Integer passengerId){
        final User passenger = userService.findById(passengerId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        return groupRepository.findAllByPassengersUser(passenger)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Group save(final Group group) {
        User foundDriver = userService.find(group.getDriver());

        if (foundDriver.isDriver()) {
            group.setDriver(foundDriver);
        } else {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        if (findOptional(group).isPresent()) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        final Address foundAddress = addressService.findOptional(group.getAddress())
                .orElseGet(() -> addressService.save(group.getAddress()));

        group.setAddress(foundAddress);

        SortedSet<UserInGroup> foundPassengers = retrievePassengers(group, group.getPassengers());
        group.setPassengers(foundPassengers);

        if (!group.getPassengers().isEmpty()) {
            routeProvider.updateGroupByOptimalRoute(group);
        }

        return groupRepository.save(group);
    }

    public Group update(final Group updatedGroup) {
        System.out.println("GroupService::update()");
        System.out.println("GroupService::update(): looking for updatedGroup...");
        final Group oldGroup = find(updatedGroup);
        System.out.println("GroupService::update(): oldGroup found");

        if (updatedGroup.getAddress() != null) {
            System.out.println("GroupService::update(): looking for updatedGroup's address...");
            updatedGroup.setAddress(addressService.findOptional(updatedGroup.getAddress()).orElseGet(() -> {
                System.out.println("GroupService::update(): updatedGroup's address not found, saving...");

                return addressService.save(updatedGroup.getAddress());
            }));
            System.out.println("GroupService::update(): updatedGroup's address sorted");
        }

        if (updatedGroup.getDriver() != null) {
            System.out.println("GroupService::update(): looking for updatedGroup's driver...");
            updatedGroup.setDriver(userService.find(updatedGroup.getDriver()));
            System.out.println("GroupService::update(): updatedGroup's driver found");

            if (!updatedGroup.getDriver().isDriver()) {
                System.out.println("GroupService::update(): ERROR: updatedGroup's driver is not a driver");
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }

        if (updatedGroup.getPassengers() != null) {
            System.out.println("GroupService::update(): retrieving passengers...");
            updatedGroup.setPassengers(retrievePassengers(oldGroup, updatedGroup.getPassengers()));
            System.out.println("GroupService::update(): passengers retrieved");
        }

        oldGroup.update(updatedGroup);

        if (oldGroup.getPassengers().size() > 0) {
            routeProvider.updateGroupByOptimalRoute(oldGroup);
        }

        return groupRepository.save(oldGroup);
    }

    public Group delete(final Group group) {
        final Group oldGroup = find(group);

        groupRepository.deleteById(oldGroup.getId());

        return oldGroup;
    }

    private SortedSet<UserInGroup> retrievePassengers(Group group, Set<UserInGroup> groupPassengers) {
        SortedSet<UserInGroup> retrievedPassengers = new TreeSet<UserInGroup>();

        for (final UserInGroup passenger : groupPassengers) {
            final User foundUser = userService.find(passenger.getUser());
            if (!foundUser.isDriver()) {
                retrievedPassengers.add(new UserInGroup(foundUser, group));
            } else {
                System.out.println("GroupService.retrievePassengers: user is driver");
                System.out.println(foundUser.getId());
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }

        return retrievedPassengers;
    }

    public Group savePassenger(final Integer groupId, final Integer userId) {
        final Group group = groupRepository.findById(groupId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        final User user = userService.findById(userId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        if (group.retrievePassengerByUser(user) != null) {
            return group;
        } else {
            final UserInGroup savedUserInGroup = userGroupRepository.save(new UserInGroup(user, group));
            group.getPassengers().add(savedUserInGroup);
            if (group.getPassengers().size() > 0) {
                routeProvider.updateGroupByOptimalRoute(group);
            }
            return groupRepository.save(group);
        }
    }

    public Group changeDepartureTime(Integer groupId, Integer userId, TimeProxy time) {
        final Group group = groupRepository.findById(groupId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        final User user = userService.findById(userId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        UserInGroup retrievedPassenger = group.retrievePassengerByUser(user);
        if (retrievedPassenger == null) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        retrievedPassenger.setDepartureTime(time.getTime());
        return groupRepository.save(group);
    }

    public Group changeReturnTime(Integer groupId, Integer userId, TimeProxy time) {
        final Group group = groupRepository.findById(groupId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        final User user = userService.findById(userId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        UserInGroup retrievedPassenger = group.retrievePassengerByUser(user);
        if (retrievedPassenger == null) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        retrievedPassenger.setReturnTime(time.getTime());
        return groupRepository.save(group);
    }

    public Group confirmDeparture(Integer groupId, Integer userId) {
        final Group group = groupRepository.findById(groupId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        final User user = userService.findById(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        UserInGroup retrievedPassenger = group.retrievePassengerByUser(user);
        if (retrievedPassenger == null) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        boolean currentConfirmState = retrievedPassenger.getDepartureConfirmed();
        retrievedPassenger.setDepartureConfirmed(!currentConfirmState);

        return groupRepository.save(group);
    }

    public Group confirmReturn(Integer groupId, Integer userId) {
        final Group group = groupRepository.findById(groupId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        final User user = userService.findById(userId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        UserInGroup retrievedPassenger = group.retrievePassengerByUser(user);
        if (retrievedPassenger == null) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        boolean currentConfirmState = retrievedPassenger.getReturnConfirmed();
        retrievedPassenger.setReturnConfirmed(!currentConfirmState);

        return groupRepository.save(group);
    }

}
