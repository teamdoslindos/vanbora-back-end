package br.gov.sp.fatec.vanborabackend.model.route;

import java.io.Serializable;
import java.util.List;

public class Leg implements Serializable {

    private static final long serialVersionUID = -5824143072796435870L;

    public Leg() {
    }

    private Summary summary;
    private List<Point> points;

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }
}
