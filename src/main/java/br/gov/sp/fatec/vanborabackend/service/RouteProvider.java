package br.gov.sp.fatec.vanborabackend.service;

import br.gov.sp.fatec.vanborabackend.model.Address;
import br.gov.sp.fatec.vanborabackend.model.Group;
import br.gov.sp.fatec.vanborabackend.model.UserInGroup;
import br.gov.sp.fatec.vanborabackend.model.route.*;
import br.gov.sp.fatec.vanborabackend.controller.RouteController;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RouteProvider {

    private final String BASE_URI = "https://api.tomtom.com/routing/1/calculateRoute/";
    private final String URI_PARAMETERS =
            "/json?key=uESO7nF9k8c37qGomNvt3MAtMe1yTcLv&traffic=true&computeBestOrder=true&arriveAt=";

    public void updateGroupByOptimalRoute(Group group) {
        SortedSet<UserInGroup> passengersToOrder = group.getPassengers();

        Route route = this.makeRouteRequest(group);
        Integer routeSeconds = route.getSummary().getTravelTimeInSeconds();

        Calendar optimalStart = (Calendar) group.getArrivalTime().clone();
        optimalStart.add(Calendar.SECOND, -routeSeconds);
        group.setOptimalStart(optimalStart);

        Calendar optimalEnd = (Calendar) group.getReturnTime().clone();
        optimalEnd.add(Calendar.SECOND, routeSeconds);
        group.setOptimalEnd(optimalEnd);

        List<PointInfo> waypoints = this.retrieveWaypoints(route);
        int orderPosition = 0;
        for(PointInfo waypoint: waypoints) {
            UserInGroup passengerClosestToPoint = pickPassengerClosestToPoint(passengersToOrder, waypoint.getPoint());
            passengerClosestToPoint.setOrderPosition(orderPosition++);

            Calendar optimalDepartureTime = (Calendar) group.getArrivalTime().clone();
            optimalDepartureTime.add(Calendar.SECOND, -waypoint.getCumulativeSecondsToPoint());
            passengerClosestToPoint.setOptimalDeparture(optimalDepartureTime);

            Calendar optimalReturnTime = (Calendar) group.getReturnTime().clone();
            optimalReturnTime.add(Calendar.SECOND, waypoint.getCumulativeSecondsToPoint());
            passengerClosestToPoint.setOptimalReturn(optimalReturnTime);
        }
    }

    private Route makeRouteRequest(Group group) {
        Calendar arrivalInstant = Calendar.getInstance();
        Calendar groupArrivalTime = (Calendar) group.getArrivalTime().clone();

        arrivalInstant.add(Calendar.DATE, 1);
        arrivalInstant.set(Calendar.HOUR, groupArrivalTime.get(Calendar.HOUR));
        arrivalInstant.set(Calendar.MINUTE, groupArrivalTime.get(Calendar.MINUTE));

        String arrivalInstantString = arrivalInstant.toInstant().toString();

        List<String> coordinatesList = new ArrayList<>();

        coordinatesList.add(formatAddressCoordinates(group.getDriver().getAddress()));

        for (UserInGroup passenger : group.getPassengers()) {
            coordinatesList.add(formatAddressCoordinates(passenger.getUser().getAddress()));
        }

        coordinatesList.add(formatAddressCoordinates(group.getAddress()));
        String coordinatesListStr = String.join(":", coordinatesList);

        String uri = BASE_URI + coordinatesListStr + URI_PARAMETERS + arrivalInstantString;
        RouteWrapper result = RouteController.makeOrderedRouteRequest(uri);

        return result.getRoutes().get(0);
    }

    private static String formatAddressCoordinates (Address address) {
        return String.valueOf(address.getLatitude()) + ',' + address.getLongitude();
    }

    private List<PointInfo> retrieveWaypoints (Route route) {
        Integer routeSeconds = route.getSummary().getTravelTimeInSeconds();
        Integer driverToPointSeconds = 0;

        List<PointInfo> waypoints = new ArrayList<>();

        int lastLegIndex = route.getLegs().size()-1;
        route.getLegs().remove(lastLegIndex);

        for (Leg currentLeg: route.getLegs()) {
            PointInfo currentPoint = new PointInfo();

            driverToPointSeconds += currentLeg.getSummary().getTravelTimeInSeconds();
            currentPoint.setCumulativeSecondsToPoint(routeSeconds - driverToPointSeconds);

            int lastLegPointIndex = currentLeg.getPoints().size() -1;
            currentPoint.setPoint(currentLeg.getPoints().get(lastLegPointIndex));

            waypoints.add(currentPoint);
        }
       return  waypoints;
    }

    private UserInGroup pickPassengerClosestToPoint (SortedSet<UserInGroup> passengers,  Point wayPoint) {
        Map<Float, UserInGroup> passengersPerDistance = new HashMap<Float, UserInGroup>();
        for (UserInGroup passenger:  passengers) {
            float latitudeDistance = wayPoint.getLatitude() - passenger.getUser().getAddress().getLatitude();
            float longitudeDistance = wayPoint.getLongitude() - passenger.getUser().getAddress().getLongitude();
            float distanceToPoint = (float) Math.sqrt((Math.pow(latitudeDistance,2) + (Math.pow(longitudeDistance,2))));
            passengersPerDistance.put(distanceToPoint, passenger);
        }
        Float shortestDistance = Collections.min(passengersPerDistance.keySet());
        return passengersPerDistance.get(shortestDistance);
    }
}
