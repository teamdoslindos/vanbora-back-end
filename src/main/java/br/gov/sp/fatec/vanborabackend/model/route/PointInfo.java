package br.gov.sp.fatec.vanborabackend.model.route;

public class PointInfo {

    private Point point;
    private Integer cumulativeSecondsToPoint;

    public PointInfo() {
    }

    public PointInfo(Point point, Integer cumulativeSecondsToPoint, Integer cumulativeReturnSeconds) {
        this.point = point;
        this.cumulativeSecondsToPoint = cumulativeSecondsToPoint;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public Integer getCumulativeSecondsToPoint() {
        return cumulativeSecondsToPoint;
    }

    public void setCumulativeSecondsToPoint(Integer cumulativeSecondsToPoint) {
        this.cumulativeSecondsToPoint = cumulativeSecondsToPoint;
    }
}
