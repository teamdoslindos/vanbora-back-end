package br.gov.sp.fatec.vanborabackend.model.comparator;

import br.gov.sp.fatec.vanborabackend.model.UserInGroup;

import java.util.Comparator;

public class UserInGroupComparator implements Comparator<UserInGroup> {

    @Override
    public int compare(UserInGroup userInGroupA, UserInGroup userInGroupB){
        if (userInGroupA.getOrderPosition() == null || userInGroupB.getOrderPosition() == null) {
           return userInGroupA.getUser().getId().compareTo(userInGroupB.getUser().getId());
        }
        return userInGroupA.getOrderPosition().compareTo(userInGroupB.getOrderPosition());
    }
}
