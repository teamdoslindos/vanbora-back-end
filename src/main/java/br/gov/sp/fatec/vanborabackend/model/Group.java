package br.gov.sp.fatec.vanborabackend.model;

import br.gov.sp.fatec.vanborabackend.model.comparator.UserInGroupComparator;
import br.gov.sp.fatec.vanborabackend.serializer.AddressDeserializer;
import br.gov.sp.fatec.vanborabackend.serializer.AddressSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.SortComparator;

import javax.persistence.*;

import java.util.Calendar;
import java.util.SortedSet;

@Entity
@Table(name = "travel_group")
public class Group {

    @Id
    // Generated value strategy required to make Hibernate let PostgreSQL
    // generate primary key.
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "driver_id")
    private User driver;

    private String name;

    @Column(name = "arrival")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", locale = "pt-BR", timezone = "Brazil/East")
    private Calendar arrivalTime;

    @Column(name = "return")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", locale = "pt-BR", timezone = "Brazil/East")
    private Calendar returnTime;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", locale = "pt-BR", timezone = "Brazil/East")
    private Calendar optimalStart;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", locale = "pt-BR", timezone = "Brazil/East")
    private Calendar optimalEnd;

    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "address_id")
    @JsonDeserialize(using = AddressDeserializer.class)
    @JsonSerialize(using = AddressSerializer.class)
    private Address address;

    @OneToMany(cascade= CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "group_id")
    @SortComparator(UserInGroupComparator.class)
    private SortedSet<UserInGroup> passengers;

    private boolean active;

    // Default contructor required by Servlet.
    public Group() {
        // Empty.
    }

    // Returns `false` if an invalid set is made.
    @JsonIgnore
    public boolean update(final Group updatedGroup) {
        if (updatedGroup.getDriver() != null) {
            this.setDriver(updatedGroup.getDriver());
        }

        if (updatedGroup.getName() != null) {
            this.setName(updatedGroup.getName());
        }

        if (updatedGroup.getArrivalTime() != null) {
            this.setArrivalTime(updatedGroup.getArrivalTime());
        }

        if (updatedGroup.getReturnTime() != null) {
            this.setReturnTime(updatedGroup.getReturnTime());
        }

        if (updatedGroup.getDescription() != null) {
            this.setDescription(updatedGroup.getDescription());
        }

        if (updatedGroup.getAddress() != null) {
            this.setAddress(updatedGroup.getAddress());
        }

        if (updatedGroup.getPassengers() != null) {
            this.setPassengers(updatedGroup.getPassengers());
        }
        
        if (updatedGroup.getActive() != getActive()) {
            this.setActive(updatedGroup.getActive());
        }

        return true;
    }

    @JsonView(View.class)
    public Integer getId() {
        return id;
    }

    @JsonView(View.class)
    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    @JsonView(View.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonView(View.class)
    public Calendar getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Calendar arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @JsonView(View.class)
    public Calendar getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Calendar returnTime) {
        this.returnTime = returnTime;
    }

    @JsonView(View.class)
    public Calendar getOptimalStart() {
        return optimalStart;
    }

    public void setOptimalStart(Calendar optimalStart) {
        this.optimalStart = optimalStart;
    }

    @JsonView(View.class)
    public Calendar getOptimalEnd() {
        return optimalEnd;
    }

    public void setOptimalEnd(Calendar optimalEnd) {
        this.optimalEnd = optimalEnd;
    }

    @JsonView(View.class)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonView(View.class)
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @JsonView(View.class)
    public SortedSet<UserInGroup> getPassengers() {
        return passengers;
    }

    // All extra passenger status, such as confirmation status, are lost!
    public void setPassengers(SortedSet<UserInGroup> passengers) {
        this.passengers = passengers;
    }

    @JsonView(View.class)
    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public interface View {
        // Empty.
    }

    public UserInGroup retrievePassengerByUser(User passengerUser){
        for(UserInGroup passengerInGroup: this.passengers) {
            if (passengerInGroup.getUser().getId().equals(passengerUser.getId())) {
                return passengerInGroup;
            }
        }
        return null;
    }
}
