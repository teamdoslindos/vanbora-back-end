package br.gov.sp.fatec.vanborabackend.serializer;

import br.gov.sp.fatec.vanborabackend.model.Address;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class AddressSerializer extends StdSerializer<Address> {

    private static final long serialVersionUID = 538948044442258506L;

    public AddressSerializer() {
        this(null);
    }

    public AddressSerializer(Class<Address> address) {
        super(address);
    }

    @Override
    public void serialize(Address address, JsonGenerator generator, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        generator.writeStartObject();

        generator.writeObjectFieldStart("address");
        generator.writeStringField("streetName", address.getStreet());
        generator.writeStringField("municipalitySubdivision", address.getNeighborhood());
        generator.writeStringField("municipality", address.getCity());
        generator.writeStringField("streetNumber", address.getNumber().toString());
        generator.writeEndObject();

        generator.writeObjectFieldStart("position");
        generator.writeNumberField("lat", address.getLatitude());
        generator.writeNumberField("lon", address.getLongitude());
        generator.writeEndObject();

        generator.writeEndObject();
    }

}
