package br.gov.sp.fatec.vanborabackend.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.gov.sp.fatec.vanborabackend.model.User;
import br.gov.sp.fatec.vanborabackend.model.Van;

public interface VanRepository extends CrudRepository<Van, Integer>{
    
    Optional<Van> findById(Integer id);
    
    Optional<Van> findByPlate(String plate);
    
    Optional<Van> deleteByPlate(String plate);

    Optional<List<Van>> findAllByDriver(User driver);

}

