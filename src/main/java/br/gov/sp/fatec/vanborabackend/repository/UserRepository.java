package br.gov.sp.fatec.vanborabackend.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.fatec.vanborabackend.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findById(Integer id);

    Optional<User> findByEmail(String email);

    Optional<User> findByPhone(String phone);

    Optional<User> findByCpf(String cpf);

    Optional<User> findByCnh(String chn);

    List<User> findByName(String name);

    List<User> findByNameContaining(String name);

    List<User> findByNameIgnoreCaseContaining(String name);

}
