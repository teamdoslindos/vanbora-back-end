package br.gov.sp.fatec.vanborabackend.model;

import javax.persistence.*;

import br.gov.sp.fatec.vanborabackend.serializer.AddressDeserializer;
import br.gov.sp.fatec.vanborabackend.serializer.AddressSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "address")
@JsonDeserialize(using = AddressDeserializer.class)
@JsonSerialize(using = AddressSerializer.class)
public class Address {

    @Id
    // Generated value strategy required to make Hibernate let PostgreSQL
    // generate primary key.
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String street;

    private String neighborhood;

    private String city;

    private Integer number;

    private Float latitude;

    private Float longitude;

    // Default contructor required by Servlet.
    public Address() {
        // Empty.
    }

    // Returns `false` if an invalid set is made.
    @JsonIgnore
    public boolean update(final Address address) {
        if (address.getStreet() != null) {
            setStreet(address.getStreet());
        }

        if (address.getNeighborhood() != null) {
            setNeighborhood(address.getNeighborhood());
        }

        if (address.getCity() != null) {
            setCity(address.getCity());
        }

        if (address.getNumber() != null) {
            setNumber(address.getNumber());
        }

        if (address.getLatitude() != null) {
            setLatitude(address.getLatitude());
        }

        if (address.getLongitude() != null) {
            setLongitude(address.getLongitude());
        }

        return true;
    }

    public Integer getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

}
