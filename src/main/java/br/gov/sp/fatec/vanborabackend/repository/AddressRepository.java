package br.gov.sp.fatec.vanborabackend.repository;

import br.gov.sp.fatec.vanborabackend.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {

    Optional<Address> findById(Integer id);

    Optional<Address> findFirstByStreetAndNumber(String street, Integer number);

}
