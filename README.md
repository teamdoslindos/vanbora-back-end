# Vanbora: Back End Edition

Este projeto e feito Java 11 com Maven e Spring e utiliza PostgreSQL.

Inicialização com o Docker Compose:

    $ mvn clean install
    $ docker-compose build
    $ docker-compose up

Inicialização com banco de dados local:

    $ # Configure a conexão com o banco em `src/main/resources/application.properties`
    $ mvn clean install
    $ mvn spring-boot:run

Rodar Testes: \
`mvn test` 

Frase de demonstração (realiza teste de acesso ao banco):
- Com a aplicação iniciada, acessar: http://localhost:8080/vanbora/hello 

Swagger:
- Com a aplicação iniciada, acessar: http://localhost:8080/vanbora/swagger-ui/index.html 
- No campo de busca "Explore", buscar: http://localhost:8080/vanbora/v3/api-docs 
